#include<stdio.h>
#include<stdlib.h>
#include<time.h>

//KEEP IT SIMPLE STUPID!

unsigned map_lenght = 36; //comprimento
unsigned map_width = 64; //largura

unsigned map_matrix[36][64];// [1080/30][1920/30] (1 tile = 30x30 px)

struct parents {
    unsigned pos[2]; // vetor2D(x,y) [Posicao inicial canto esquerdo superior]
    unsigned len;
    unsigned wid;
} p1, p2, c1, c2, c3, c4, c5, c6, c7, c8, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16;

void print_logo(void) {
    printf("\n _____ _____ _____ _____    _____ _____ _____    _____ __ __ _____ _____ _____ _____ ");
    printf("\n|   __|     | __  |  |  |  |_   _|  |  |   __|  |   __|  |  |   __|_   _|   __|     |");
    printf("\n|   __|  |  |    -|    -|    | | |     |   __|  |__   |_   _|__   | | | |   __| | | |");
    printf("\n|__|  |_____|__|__|__|__|    |_| |__|__|_____|  |_____| |_| |_____| |_| |_____|_|_|_|");
    printf("\n\nv0.3\n");
}

void fill_with_zero(void) {
    unsigned i, j;
    for (i=0; i < map_lenght; i++) {
        for (j=0; j < map_width; j++) {
            map_matrix[i][j] = 0; //rand() % 10;
        }
    }
}

void print_matrix(void) {
    unsigned i, j;
    for (i=0; i < map_lenght; i++) {
        for (j=0; j < map_width; j++) {
            printf("%u ", map_matrix[i][j]);
        }
        printf("\n");
    }
}

void draw_floor(unsigned pos_xy[], unsigned len, unsigned wid) {
    unsigned i, j;
    for (i = pos_xy[1] + 1; i < pos_xy[1] + len; i++) { //desenha o chao
        for (j = pos_xy[0] + 1; j < pos_xy[0] + wid; j++) {
            map_matrix[i][j] = 2;
        }
   } 
}
    
void draw_room(unsigned pos_xy[], unsigned len, unsigned wid) {
    len = len -1; //array comeca no zero
    wid = wid -1;
    unsigned i, j;
    for (i = pos_xy[0]; i <= pos_xy[0] + wid; i++) { //desenha linhas horizontais
        map_matrix[pos_xy[1]][i] = 1;
        map_matrix[pos_xy[1] + len][i] = 1;
    }
    for (i = pos_xy[1]; i <= pos_xy[1] + len; i++) { //desenha linhas verticais
        map_matrix[i][pos_xy[0]] = 1;
        map_matrix[i][pos_xy[0] + wid] = 1;
    }
    //draw_floor(pos_xy, len, wid);
}

void cut_in_half(struct parents *pa1, struct parents *pa2, unsigned mode) {
    pa1->pos[0] = 0;
    pa1->pos[1] = 0;

    if (mode == 0) { //corte horizontal
        pa1->len = map_lenght/2;
        pa1->wid = map_width;
        pa2->pos[0] = 0;
        pa2->pos[1] = map_lenght/2;
        pa2->len = map_lenght/2;
        pa2->wid = map_width;
    }
    else { //corte vertical
        pa1->len = map_lenght;
        pa1->wid = map_width/2;
        pa2->pos[0] = map_width/2;
        pa2->pos[1] = 0;
        pa2->len = map_lenght;
        pa2->wid = map_width/2;
    }
}

//Passar sala a ser dividida e as 2 salas que serao geradas e o modo (0 = hor, 1 = ver)
//Nao repetir o mesmo endereco de memoria ou bug (wid ou len = 0).

void divide_cell(struct parents *pa1, struct parents *ce1, struct parents *ce2, unsigned mode) {
    ce1->pos[0] = pa1->pos[0];//cel1 sempre vai ter xy do pai.
    ce1->pos[1] = pa1->pos[1];

    if (mode == 0) { //corte horizontal
        //rand() % (max_value - min_value + 1) + min_value; (para gerar no range)
        ce1->len = rand() % ((pa1->len /3*2) - (pa1->len/3) + 1) + pa1->len/3;
        ce1->wid = pa1->wid; 

        ce2->pos[0] = pa1->pos[0];
        ce2->pos[1] = pa1->pos[1] + ce1->len;
        ce2->len = pa1->len - ce1->len;
        ce2->wid = pa1->wid;
    }
    else { //corte vertical
        ce1->len = pa1->len;
        ce1->wid = rand() % ((pa1->wid /3*2) - (pa1->wid/3) + 1) + pa1->wid/3;

        ce2->pos[0] = pa1->pos[0] + ce1->wid;
        ce2->pos[1] = pa1->pos[1];
        ce2->len = pa1->len;
        ce2->wid = pa1->wid - ce1->wid;
    }
}

void make_cells(void) {
    unsigned coin = rand() % 2;

    //primeiro corte (vertical ou horizontal)
    cut_in_half(&p1, &p2, coin);

    //segundo corte (p1 vira c1 e c3)
    if (coin == 0) {
        divide_cell(&p1, &c1, &c3, 1);
        divide_cell(&p2, &c2, &c4, 1);
    }
    else {
        divide_cell(&p1, &c1, &c3, rand() % 2);
        divide_cell(&p2, &c2, &c4, rand() % 2);

    }

    draw_room(c1.pos, c1.len, c1.wid);
    draw_room(c2.pos, c2.len, c2.wid);
    draw_room(c3.pos, c3.len, c3.wid);
    draw_room(c4.pos, c4.len, c4.wid);
}

int main(void) {
    //srand(666);
    srand(time(0)); //UNIX time
    print_logo();

    printf("\nMap Lenght: %u\n", map_lenght);
    printf("Map width: %u\n\n", map_width);

    fill_with_zero();
    make_cells();
    print_matrix();
    return 0;
}
