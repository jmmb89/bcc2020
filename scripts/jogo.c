#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>

/* ### GAME VARIABLES ### */

float FPS =  60;
unsigned LARGURA_TELA = 1920;
unsigned ALTURA_TELA = 1080;

ALLEGRO_DISPLAY *janela = NULL;
ALLEGRO_EVENT_QUEUE *fila_eventos = NULL;
ALLEGRO_TIMER *timer = NULL;
ALLEGRO_BITMAP *tile_sprite = NULL;

/* MENU */
ALLEGRO_BITMAP *title = NULL;
ALLEGRO_FONT *font = NULL;
ALLEGRO_FONT *font2 = NULL;
ALLEGRO_SAMPLE *blup = NULL;
ALLEGRO_SAMPLE *blip = NULL;

/* ### DUNGEON VARIABLES ### */

unsigned map_lenght = 36; //comprimento
unsigned map_width = 64; //largura

unsigned map_matrix[36][64];// [1080/30][1920/30] (1 tile = 30x30 px)

struct parents {
    unsigned pos[2]; // vetor2D(x,y) [Posicao inicial canto esquerdo superior]
    unsigned len;
    unsigned wid;
} p1, p2, c1, c2, c3, c4, pr1, pr2, pr3, pr4, pr5, pr6, pr7, pr8;

struct rooms {
    unsigned pos[2];
    unsigned len;
    unsigned wid;
} r1, r2, r3, r4, r5, r6, r7, r8;

/* DUNGEON FUNCTIONS */

void fill_with_zero() {
  unsigned i, j;
  for (i=0; i < map_lenght; i++) {
    for (j=0; j < map_width; j++) {
      map_matrix[i][j] = 0; //rand() % 10;
    }
  }
}

void draw_floor(unsigned pos_xy[], unsigned len, unsigned wid) {
  unsigned i, j;
  for (i = pos_xy[1] + 1; i < pos_xy[1] + len; i++) {
    for (j = pos_xy[0] + 1; j < pos_xy[0] + wid; j++) {
      map_matrix[i][j] = 2;
    }
  } 
}

void draw_room(unsigned pos_xy[], unsigned len, unsigned wid) {
  len = len -1; //array comeca no zero
  wid = wid -1;
  unsigned i, j;
  for (i = pos_xy[0]; i <= pos_xy[0] + wid; i++) { //desenha linhas horizontais
    map_matrix[pos_xy[1]][i] = 1;
    map_matrix[pos_xy[1] + len][i] = 1;
  }
  for (i = pos_xy[1]; i <= pos_xy[1] + len; i++) { //desenha linhas verticais
    map_matrix[i][pos_xy[0]] = 1;
    map_matrix[i][pos_xy[0] + wid] = 1;
  }
  draw_floor(pos_xy, len, wid);
}

void cut_in_half(struct parents *pa1, struct parents *pa2, unsigned mode) {
  pa1->pos[0] = 0;
  pa1->pos[1] = 0;

  if (mode == 0) { //corte horizontal
    pa1->len = map_lenght/2;
    pa1->wid = map_width;
    pa2->pos[0] = 0;
    pa2->pos[1] = map_lenght/2;
    pa2->len = map_lenght/2;
    pa2->wid = map_width;
  }
  else { //corte vertical
    pa1->len = map_lenght;
    pa1->wid = map_width/2;
    pa2->pos[0] = map_width/2;
    pa2->pos[1] = 0;
    pa2->len = map_lenght;
    pa2->wid = map_width/2;
  }
}

//Passar sala a ser dividida e as 2 salas que serao geradas e o modo (0 = hor, 1 = ver)
//Nao repetir o mesmo endereco de memoria ou bug (wid ou len = 0).

void divide_cell(struct parents *pa1, struct parents *ce1, struct parents *ce2, unsigned mode) {
  ce1->pos[0] = pa1->pos[0];//ce1 sempre vai ter xy do pai.
  ce1->pos[1] = pa1->pos[1];

  if (mode == 0) { //corte horizontal
    //rand() % (max_value - min_value + 1) + min_value; (para gerar no range)
    ce1->len = rand() % ((pa1->len /3*2) - (pa1->len/3) + 1) + pa1->len/3;
    ce1->wid = pa1->wid; 

    ce2->pos[0] = pa1->pos[0];
    ce2->pos[1] = pa1->pos[1] + ce1->len;
    ce2->len = pa1->len - ce1->len;
    ce2->wid = pa1->wid;
  }
  else { //corte vertical
    ce1->len = pa1->len;
    ce1->wid = rand() % ((pa1->wid /3*2) - (pa1->wid/3) + 1) + pa1->wid/3;
    ce2->pos[0] = pa1->pos[0] + ce1->wid;
    ce2->pos[1] = pa1->pos[1];
    ce2->len = pa1->len;
    ce2->wid = pa1->wid - ce1->wid;
  }
}

//talvez seja desnecessario. (refletir)
unsigned choose_cut(struct parents *pa) {
  if (pa->len >= pa->wid) {
    return 0;
  }
  else {
    return 1;
  }
}

void make_cells() {
  unsigned coin = rand() % 2;
  //primeiro corte (vertical ou horizontal)
  cut_in_half(&p1, &p2, coin);

  //segundo corte (p1 => c1 e c3, p2 => c2 e c4)
  if (coin == 0) {
    divide_cell(&p1, &c1, &c3, 1);
    divide_cell(&p2, &c2, &c4, 1);
  }
  else {
    divide_cell(&p1, &c1, &c3, rand() % 2);
    divide_cell(&p2, &c2, &c4, rand() % 2);
  }

  divide_cell(&c1, &pr1, &pr3, choose_cut(&c1));
  divide_cell(&c2, &pr2, &pr4, choose_cut(&c2));
  divide_cell(&c3, &pr5, &pr7, choose_cut(&c3));
  divide_cell(&c4, &pr6, &pr8, choose_cut(&c4));
  /*
  divide_cell(&c1, &pr1, &pr3, rand() % 2);
  divide_cell(&c2, &pr2, &pr4, rand() % 2);
  divide_cell(&c3, &pr5, &pr7, rand() % 2);
  divide_cell(&c4, &pr6, &pr8, rand() % 2);
  */
  draw_room(pr1.pos, pr1.len, pr1.wid);
  draw_room(pr2.pos, pr2.len, pr2.wid);
  draw_room(pr3.pos, pr3.len, pr3.wid);
  draw_room(pr4.pos, pr4.len, pr4.wid);
  draw_room(pr5.pos, pr5.len, pr5.wid);
  draw_room(pr6.pos, pr6.len, pr6.wid);
  draw_room(pr7.pos, pr7.len, pr7.wid);
  draw_room(pr8.pos, pr8.len, pr8.wid);
}

/* ### GAME FUNCTIONS ### */

void desenha_dungeon() {
  unsigned i, j, pos_x_folha, pos_y_folha = 0, pos_x_tela, pos_y_tela;
  unsigned altura_sprite = 30, largura_sprite = 30; //30x30 = tamanho do tile.
  for (i=0; i < map_lenght; i++) {
    for (j=0; j < map_width; j++) {
      pos_x_folha = map_matrix[i][j] * 30;
      pos_x_tela = j * 30;
      pos_y_tela = i * 30;
      al_draw_bitmap_region(tile_sprite, pos_x_folha, pos_y_folha, largura_sprite, altura_sprite, pos_x_tela, pos_y_tela, 0);
    }
  }
}

int inicializar() {
  if (!al_init()) {
    printf("\nFalha ao iniciar o allegro");
    return 1;
  }

  if (!al_init_image_addon()) {
    printf("\nFalha ao inicializar addon de imagem\n");
    return 1;
  }

  if (!al_init_primitives_addon()) {
    printf("\nFalha ao inicializar addon primitives\n");
    return 1;
  }

  if (!al_init_font_addon()) {
    printf("\nFalha ao inicializar addon font\n");
    return 1;
  }

  if (!al_init_ttf_addon()) {
    printf("\nFalha ao inicializar addon ttf\n");
    return 1;
  }

  if (!al_install_keyboard()) {
    printf("\nFalha ao inicializar teclado\n");
    return 1;
  }

  if (!al_init_acodec_addon()) {
    printf("\nFalha ao inicializar addon acodec\n");
    return 1;
  }

  if (!al_install_audio()) {
    printf("\nFalha ao inicializar audio\n");
    return 1;
  }

  timer = al_create_timer(0.3);
  if (!timer) {
    printf("\nFalha ao criar timer\n");
    return 1;
  }

  //al_set_new_display_flags(ALLEGRO_FULLSCREEN_WINDOW); //deixa full secreen
  janela = al_create_display(LARGURA_TELA, ALTURA_TELA);
  if (!janela) {
    printf("\nFalha ao criar janela\n");
    al_destroy_timer(timer);
    return 1;
  }

  al_set_window_title(janela, "GRUPO HAVANA");//define o titulo da janela.

  fila_eventos = al_create_event_queue();
  if (!fila_eventos) {
    printf("\nFalha ao criar fila de eventos\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    return 1;
  }
  /* RESOURCES */
  tile_sprite = al_load_bitmap("../images/tiles.jpg");
  title = al_load_bitmap("../images/bg_main2.png");
  font = al_load_font("../fonts/godofwar.ttf", 54, 0);
  font2 = al_load_font("../fonts/godofwar.ttf", 40, 0);
  blup = al_load_sample("../audio/blup.ogg");
  blip = al_load_sample("../audio/blip.ogg");

  if (!tile_sprite) {
    printf("\nFalha ao carregar tiles\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    return 1;
  }
   
  if (!title) {
    printf("Falha ao carregar main bg image\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    al_destroy_bitmap(tile_sprite); 
    return 1;
  }

  if (!font) {
    printf("\nFlaha ao carregar font 1\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    al_destroy_bitmap(tile_sprite); 
    al_destroy_bitmap(title);
    return 1;
  }

  if (!font2) {
    printf("\nFalha ao carregar font 2\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    al_destroy_bitmap(tile_sprite); 
    al_destroy_bitmap(title);
    al_destroy_font(font);
    return 1;
  }

  if (!blup) {
    printf("\nFalha ao carregar som blup\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    al_destroy_bitmap(tile_sprite); 
    al_destroy_bitmap(title);
    al_destroy_font(font);
    al_destroy_font(font2);
    return 1;
  }
  if (!blip) {
    printf("\nFalha ao carregar som blip\n");
    al_destroy_timer(timer);
    al_destroy_display(janela);
    al_destroy_event_queue(fila_eventos);
    al_destroy_bitmap(tile_sprite); 
    al_destroy_bitmap(title);
    al_destroy_font(font);
    al_destroy_font(font2);
    al_destroy_sample(blup);
    return 1;
  }
    
  al_reserve_samples(1);

  //al_convert_mask_to_alpha(tile_sprite, al_map_rgb(255, 0 , 255)) // usa a cor rosa como transparencia.

  al_register_event_source(fila_eventos, al_get_display_event_source(janela));
  al_register_event_source(fila_eventos, al_get_keyboard_event_source());
  al_register_event_source(fila_eventos, al_get_timer_event_source(timer));
  al_start_timer(timer);


  return 0;
}

void draw_menu(int mode, int pisca) {
  ALLEGRO_COLOR branco = al_map_rgb(255, 255, 255);
  ALLEGRO_COLOR amarelo = al_map_rgb(166, 145, 129);
  al_clear_to_color(al_map_rgb(0, 0, 0));
  al_draw_bitmap(title, 0, 0, 0);
  al_draw_text(font, branco, 162, 141, 0, "Projeto Havana");
  al_draw_text(font2, amarelo, 297, 363, 0, "Start Game");
  al_draw_text(font2, amarelo, 311, 523, 0, "Options");
  if (pisca == 0) {
    if (mode == 0) {
      al_draw_rectangle(291, 356, 520, 414, branco, 3);
    }
    else {
      al_draw_rectangle(293, 515, 507, 573, branco, 3);
    }
  }
  al_flip_display();
}

void desenha_mapa() {
  desenha_dungeon();
  al_flip_display();
}

int main(void) {
  int jogando = 1;
  int pisca = 0;
  int cont_frames = 0;
  int main_menu = 1;
  int menu_opt = 0;

  if (inicializar()) { //se retornar 1 eh pq deu ruim.
    return 1;
  }
	
  //srand(666);
  srand(time(0)); //UNIX time
  fill_with_zero();
  make_cells();
  draw_menu(0, pisca);

  while (jogando) {
    ALLEGRO_EVENT evento;
    ALLEGRO_TIMEOUT timeout;
    al_init_timeout(&timeout, 0.05);
    int tem_eventos = al_wait_for_event_until(fila_eventos, &evento, &timeout);
    
    /* ### MENU INPUTS ### */ //REFAZER COM CASE?
    if (main_menu) {
      if (tem_eventos && evento.type == ALLEGRO_EVENT_TIMER) {
        if (pisca == 0) {
          pisca = 1;
        }
        else {
          pisca = 0;
        }
        draw_menu(menu_opt, pisca);
      }
      if (tem_eventos && evento.type == ALLEGRO_EVENT_KEY_DOWN) {
        if (evento.keyboard.keycode == ALLEGRO_KEY_DOWN) {
          if (menu_opt == 0) {
            menu_opt = 1;
          }
          else {
            menu_opt = 0;
          }
          draw_menu(menu_opt, 0);
          al_play_sample(blup, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);

        }
        else {
          if (evento.keyboard.keycode == ALLEGRO_KEY_UP) {
            if (menu_opt == 0) {
              menu_opt = 1;
            }
            else {
              menu_opt = 0;
            }
            draw_menu(menu_opt, 0);
            al_play_sample(blup, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
          }
          else {
            if (evento.keyboard.keycode == ALLEGRO_KEY_ENTER) {
              if (menu_opt == 0) {
                al_play_sample(blip, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, NULL);
                main_menu = 0;
                desenha_mapa();
                al_destroy_timer(timer);//limpa itens menu da memoria.
                al_destroy_bitmap(title);
                al_destroy_font(font);
                al_destroy_font(font2);
                al_destroy_sample(blup);
                al_destroy_sample(blip);
              }
            }
          }
        }
      }
    }

    if (tem_eventos && evento.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
      jogando = 0;
    }
  }

  /* SEEK && DESTROY */
  al_destroy_bitmap(tile_sprite);
  al_destroy_display(janela);
  al_destroy_event_queue(fila_eventos);
  
  if (main_menu) {
    al_destroy_timer(timer);
    al_destroy_bitmap(title);
    al_destroy_font(font);
    al_destroy_font(font2);
    al_destroy_sample(blup);
    al_destroy_sample(blip);
  }

  return 0;
}
