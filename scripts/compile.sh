#!/bin/bash

#uso: colocar na mesma pasta do arquivo .c e passar como argumento o
#nome do arquivo .c e o nome do binario que voce quer gerar.

info=$(cat $1 | grep allegro5 | cut -d '.' -f1 | cut -d '/' -f2)

declare -a libs 

for i in $info; do
  libs+=(-l$i)
done

gcc $1 -o $2 ${libs[*]}
