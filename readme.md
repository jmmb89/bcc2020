# FORK THE SYSTEM

### Grupo: Havana


|         A FAZER                 |  PRAZO  | RESPONSÁVEL |
|---------------------------------|:-------:|------------:|
|merge e sistema de falhas        | 13/10   | Joao        |
|tiles e movimento player         | 13/10   | Leandro     |


|			FEITO		                    |  DATA  | RESPONSÁVEL |
|---------------------------------|:-------|------------:|
|implementar algoritimo (allegro) | 06/10  | Joao        |
|tela inicial e botoes            | 06/10  | Leandro     |
|implementar algoritimo mapa      | 29/09  | Joao        |
|tela inicial                     | 29/09  | Leandro     |
|ideia algoritimo de criacao mapa | 22/09  | Joao		     |
|ideia tela inicial do jogo       | 22/09  | Leandro     |
